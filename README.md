# Arudino Hockey Table Fix

We received a great free hockey table, but the electronics had since stopped working. While taking it apart, the main breadboard had been busted. I sought out to backwards engineer the needed electronics parts (scoring sensor, scoreboard, LEDs) and mimic their functionality with an Arduino Nano and Arduino Uno.

## Parts needing fixing

- **Blue Outlining LEDs Across Rink:** These lights are made purely for looks lining the bumpers of the table. As these are just LED strips, I spliced the power cord running to the main (now busted) board and powered the LED strips with it. As the LEDs only needed a 12V source to power, I made quick work with some soldering and connectors to get the strips lit.

- **Scoreboard:** The scoreboard was the most difficult piece to figure out. It had a 12 pin connector that powered the top 8 LEDs, as well as both home and away scores and the timer. After debugging the pins, the pattern used was figured out and code was written for the Arduino Nano to power each segment of the scores and the top 8 LEDs. I decided I didn't really care for the timer in an air hockey game. One side also had a reset button for scores that I was able to figure out and code up.
    For a quick explanation of the 12 pin connector: 8 of the pins were used for the 7 segments of a digital digit (The eighth pin was needed for the additional top LED). The other 4 pins were used as 'control' pins. Whichever 'control' pin was powered determined which score or if the top 8 LEDs were powered. A quick cycle with short delays between the 3 on the Nano allowed for each to get its value and blink fast enough to appear solid to the human eye.

- **Puck Sensors:** The puck sensors made use of an IR sensor and emittor. This tripped me up a bit as I was used to having IR sensors or break beam sensors with at least 3 pins. The emittor and sensor both only had 2 pins and just worked as a basic sensor and emittor which was admittedly easier. After some troubleshooting to find the threshold for both the visitor and home sensors, code was written for the Arduino Nano to detect the pucks and up the scores.

- **Scoring Sounds:** I had run out of pins and memory space on my Arduino Nano which is the main brain, so an an old Arduino Uno was hooked up over Serial with a speaker to hold scoring and game-end sounds. The Nano either sends a 1 or a 2 to trigger the sound. A hockey buzzer for a score and "Thunderstruck" by AC/DC for winning!

## Additional Components - HW & SW
- [Arduino Nano](https://store.arduino.cc/usa/arduino-nano) - Programmed with `src/AirHockeyScorer.ino`
- [Arduino Uno](https://store.arduino.cc/usa/arduino-uno-rev3) - Programmed with `src/UnoSoundControl/HockeySound.ino`
- 8 ohm Speaker w/ H-Bridge to Amplify Sound
- [High-Low Tech Audio Library](http://highlowtech.org/?p=1963) and Tools to Convert Sound and Play through Arduino Digital Pins

## Future Ideas
- Longer and louder sounds from speaker
- Connect outlining blue LEDs to Arduinos (Arduini??) to flash on scores
