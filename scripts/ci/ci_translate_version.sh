#!/bin/bash

# Script used for translating .version into a readable header file for the application

read -r line<.version
printf "#define FIRMWARE_VERSION \"%s\"" ${line//[$'\t\r\n ']} > sources/application/inc/build.h
