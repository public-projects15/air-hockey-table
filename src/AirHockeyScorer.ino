/*
AirHockeyScorer.ino
Cole Spear
*/

#define delays 6500

// Digital and Analog Pin Definitions
int segA = 2; 
int segB = 3; 
int segC = 4; 
int segD = 5; 
int segE = 6;
int segF = 7; 
int segG = 8; 
int visitorCon = 9;
int homeCon = 10;
int topLedCon = 11;
int lastLED = 12;
int resetButton = 13;
const int lightSensorPinHome = A0;
const int lightSensorPinVisitor = A1;

//Variable Declarations
int visitorScore = 0;
int homeScore = 0;
int topLEDs = 10;
int sensorCount = 0;
int puckRead = false;


void setup() {
  pinMode(segA, OUTPUT);
  pinMode(segB, OUTPUT);
  pinMode(segC, OUTPUT);
  pinMode(segD, OUTPUT);
  pinMode(segE, OUTPUT);
  pinMode(segF, OUTPUT);
  pinMode(segG, OUTPUT);
  pinMode(visitorCon, OUTPUT);
  pinMode(homeCon, OUTPUT);
  pinMode(topLedCon, OUTPUT);
  pinMode(lastLED, OUTPUT);
  pinMode(resetButton, INPUT_PULLUP);

  Serial.begin(9600);
}

void loop() {
  
  // When physical reset button is pressed
  if(digitalRead(resetButton) == 1) {
    resetScores();
  }

  writeVisitorNumber(visitorScore);
  delayMicroseconds(delays);
  writeHomeNumber(homeScore);
  delayMicroseconds(delays);

  // Handle Score
  if(puckRead == true){
    if(sensorCount >= 50){
      puckRead = false;
      sensorCount = 0;
    }
    else{
      writeTopLEDs(sensorCount/10);
      sensorCount++;
    }
  }
  else{
    readGoals();
    writeTopLEDs(topLEDs);
  }

  // End game logic
  if(homeScore >= 10 || visitorScore >= 10)
  {
    // Write to Arduino Uno
    Serial.write(2);
    resetScores();
  }
  delayMicroseconds(delays);
}

// Read goals that are using IR sensors to
// detect puck
void readGoals() {
  int sensorValueHome = analogRead(lightSensorPinHome);
  int sensorValueVisitor = analogRead(A1);
  if(sensorValueHome <= 20){
    puckRead = true;
    homeScore++;
    // Write to Arduino Uno
    Serial.write(1);
  }
  if(sensorValueVisitor <= 3){
    puckRead = true;
    visitorScore++;
    // Write to Arduino Uno
    Serial.write(1);
  }
}

void writeVisitorNumber(int visitorScore) {
  lightNumber(visitorScore);
  digitalWrite(visitorCon, HIGH);
  digitalWrite(homeCon,    LOW);
  digitalWrite(topLedCon,  LOW);
}

void writeHomeNumber(int homeScore) {
  lightNumber(homeScore);
  digitalWrite(visitorCon, LOW);
  digitalWrite(homeCon,    HIGH);
  digitalWrite(topLedCon,  LOW);
}

// Decorative LEDs on top of scoreboard -
// Same write functionality as visitor
// and home score
void writeTopLEDs(int topLEDs) {
  lightNumber(topLEDs);
  digitalWrite(visitorCon, LOW);
  digitalWrite(homeCon,    LOW);
  digitalWrite(topLedCon,  HIGH);
}

void resetScores() {
  homeScore = 0;
  visitorScore = 0;
}

void lightNumber(int numberToDisplay) {
#define SEGMENT_ON  LOW
#define SEGMENT_OFF HIGH
  // Light correct segments of single
  // digital digit scoreboard
  switch (numberToDisplay){
  case 0:
    digitalWrite(segA, SEGMENT_ON);
    digitalWrite(segB, SEGMENT_ON);
    digitalWrite(segC, SEGMENT_ON);
    digitalWrite(segD, SEGMENT_ON);
    digitalWrite(segE, SEGMENT_ON);
    digitalWrite(segF, SEGMENT_ON);
    digitalWrite(segG, SEGMENT_OFF);
    break;
  case 1:
    digitalWrite(segA, SEGMENT_OFF);
    digitalWrite(segB, SEGMENT_ON);
    digitalWrite(segC, SEGMENT_ON);
    digitalWrite(segD, SEGMENT_OFF);
    digitalWrite(segE, SEGMENT_OFF);
    digitalWrite(segF, SEGMENT_OFF);
    digitalWrite(segG, SEGMENT_OFF);
    break;
  case 2:
    digitalWrite(segA, SEGMENT_ON);
    digitalWrite(segB, SEGMENT_ON);
    digitalWrite(segC, SEGMENT_OFF);
    digitalWrite(segD, SEGMENT_ON);
    digitalWrite(segE, SEGMENT_ON);
    digitalWrite(segF, SEGMENT_OFF);
    digitalWrite(segG, SEGMENT_ON);
    break;
  case 3:
    digitalWrite(segA, SEGMENT_ON);
    digitalWrite(segB, SEGMENT_ON);
    digitalWrite(segC, SEGMENT_ON);
    digitalWrite(segD, SEGMENT_ON);
    digitalWrite(segE, SEGMENT_OFF);
    digitalWrite(segF, SEGMENT_OFF);
    digitalWrite(segG, SEGMENT_ON);
    break;
  case 4:
    digitalWrite(segA, SEGMENT_OFF);
    digitalWrite(segB, SEGMENT_ON);
    digitalWrite(segC, SEGMENT_ON);
    digitalWrite(segD, SEGMENT_OFF);
    digitalWrite(segE, SEGMENT_OFF);
    digitalWrite(segF, SEGMENT_ON);
    digitalWrite(segG, SEGMENT_ON);
    break;
  case 5:
    digitalWrite(segA, SEGMENT_ON);
    digitalWrite(segB, SEGMENT_OFF);
    digitalWrite(segC, SEGMENT_ON);
    digitalWrite(segD, SEGMENT_ON);
    digitalWrite(segE, SEGMENT_OFF);
    digitalWrite(segF, SEGMENT_ON);
    digitalWrite(segG, SEGMENT_ON);
    break;
  case 6:
    digitalWrite(segA, SEGMENT_ON);
    digitalWrite(segB, SEGMENT_OFF);
    digitalWrite(segC, SEGMENT_ON);
    digitalWrite(segD, SEGMENT_ON);
    digitalWrite(segE, SEGMENT_ON);
    digitalWrite(segF, SEGMENT_ON);
    digitalWrite(segG, SEGMENT_ON);
    break;
  case 7:
    digitalWrite(segA, SEGMENT_ON);
    digitalWrite(segB, SEGMENT_ON);
    digitalWrite(segC, SEGMENT_ON);
    digitalWrite(segD, SEGMENT_OFF);
    digitalWrite(segE, SEGMENT_OFF);
    digitalWrite(segF, SEGMENT_OFF);
    digitalWrite(segG, SEGMENT_OFF);
    break;
  case 8:
    digitalWrite(segA, SEGMENT_ON);
    digitalWrite(segB, SEGMENT_ON);
    digitalWrite(segC, SEGMENT_ON);
    digitalWrite(segD, SEGMENT_ON);
    digitalWrite(segE, SEGMENT_ON);
    digitalWrite(segF, SEGMENT_ON);
    digitalWrite(segG, SEGMENT_ON);
    break;
  case 9:
    digitalWrite(segA, SEGMENT_ON);
    digitalWrite(segB, SEGMENT_ON);
    digitalWrite(segC, SEGMENT_ON);
    digitalWrite(segD, SEGMENT_ON);
    digitalWrite(segE, SEGMENT_OFF);
    digitalWrite(segF, SEGMENT_ON);
    digitalWrite(segG, SEGMENT_ON);
    break;
  case 10:
    digitalWrite(segA, SEGMENT_ON);
    digitalWrite(segB, SEGMENT_ON);
    digitalWrite(segC, SEGMENT_ON);
    digitalWrite(segD, SEGMENT_ON);
    digitalWrite(segE, SEGMENT_ON);
    digitalWrite(segF, SEGMENT_ON);
    digitalWrite(segG, SEGMENT_ON);
    digitalWrite(lastLED, SEGMENT_ON);
    break;
  case 11:
    digitalWrite(segA, SEGMENT_OFF);
    digitalWrite(segB, SEGMENT_OFF);
    digitalWrite(segC, SEGMENT_OFF);
    digitalWrite(segD, SEGMENT_OFF);
    digitalWrite(segE, SEGMENT_OFF);
    digitalWrite(segF, SEGMENT_OFF);
    digitalWrite(segG, SEGMENT_OFF);
    digitalWrite(lastLED, SEGMENT_ON);
    break;
  }
 
}
